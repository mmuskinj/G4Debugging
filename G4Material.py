from copy import deepcopy


unitMap = {
  "cm":10000.,
  "mm":1000.,
  "um":1.,
  "MeV":1000000.,
  "keV":1000.,
  "eV":1.,
}

def getMaterials(f):
  materials = {}
  materialLine = 0
  index = -1
  for x in f:
    a = x.split()
    if "used in the geometry" in x:
      # print "- Found new material, index: ", a[4]
      materialLine += 1
      mat = deepcopy(G4Material(index=a[4]))
      materials[a[4]] = mat
      index = a[4]
      continue
    elif materialLine == 1:
      # print "- named: ", a[4]
      materialLine += 1
      materials[index].setName(a[4])
      continue
    elif materialLine == 2:
      assert len(a) == 17, len(a)
      assert a[5] == "gamma"
      assert a[8] == "e-"
      assert a[11] == "e+"
      assert a[14] == "proton"
      # print "- Range Cuts:"
      for i in range(4):
        # print "%6s: %10.3f um" % (a[5+3*i],unitMap[a[7+3*i]]*float(a[6+3*i]))
        materials[index].rangeCuts[a[5+3*i]] = unitMap[a[7+3*i]]*float(a[6+3*i])
      materialLine += 1
      continue
    elif materialLine == 3:
      assert len(a) == 17, len(a)
      assert a[5] == "gamma"
      assert a[8] == "e-"
      assert a[11] == "e+"
      assert a[14] == "proton"
      # print "- Energy Thresholds:"
      for i in range(4):
        # print "%6s: %10.3f eV" % (a[5+3*i],unitMap[a[7+3*i]]*float(a[6+3*i]))
        materials[index].energyThresholds[a[5+3*i]] = unitMap[a[7+3*i]]*float(a[6+3*i])
      materialLine += 1
      continue
    elif materialLine == 4:
      assert a[2]=="Region(s)"
      materialLine += 1
    elif materialLine == 5:
      # print "- Regions: ", a[2:]
      # print "-----------------"
      materials[index].regions = a[2:]
      materialLine += 1
    else:
      materialLine = 0
      index = -1
  return materials


class G4Material(object):
    index = None
    name = ""
    rangeCuts = {}
    energyThresholds = {}
    regions = []

    def __init__(self, index = None, name = None, rangeCuts = {}, energyThresholds = {}, regions = []):
        self.index = index
        self.name = name
        self.rangeCuts = rangeCuts
        self.energyThresholds = energyThresholds
        self.regions = regions

    def __repr__(self):
      string = "%s %s \n" % (self.name, self.index)
      string += "== Range cuts:\n"
      for r in self.rangeCuts:
        string += "%6s: %10.3f um\n" % (r,self.rangeCuts[r])
      string += "== Energy thresholds:\n"
      for e in self.energyThresholds:
        string += "%6s: %10.3f eV\n" % (e,self.energyThresholds[e])
      string += "== Regions:\n"
      for r in self.regions:
        string += "%6s \n" % r
      return string

    def getEnergyThresholdsString(self):
      string = "%f %f %f %f" % (self.energyThresholds["gamma"],
                                self.energyThresholds["e-"],
                                self.energyThresholds["e+"],
                                self.energyThresholds["proton"]
                                )
      return string


    def setIndex(self, index):
        self.index = index

    def setName(self, name):
        self.name = name

    def setRangeCuts(self, rangeCuts):
        self.rangeCuts = rangeCuts

    def setEnergyThresholds(self, energyThresholds):
        self.energyThresholds = energyThresholds

    def setRegions(self, regions):
        self.regions = regions

