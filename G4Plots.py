import sys
sys.path.append('./')

import ROOT
import os
import operator

import G4Material
from G4Material import unitMap, getMaterials

from copy import deepcopy
from collections import defaultdict

ROOT.gROOT.SetBatch(True)

AtlasStylePath = os.environ['HOME']+'/RootUtils/AtlasStyle'
AtlasStyle = os.path.exists(AtlasStylePath)
if AtlasStyle:
  print "setting ATLAS Style"
  ROOT.gROOT.LoadMacro(AtlasStylePath+"/AtlasStyle.C")
  ROOT.gROOT.LoadMacro(AtlasStylePath+"/AtlasLabels.C")
  ROOT.gROOT.LoadMacro(AtlasStylePath+"/AtlasUtils.C")
  ROOT.SetAtlasStyle()

particles = [
'e-',
'e+',
'gamma',
'neutron',
'other',
]
colors = {
 'e-':ROOT.kBlue-2,
 'e+':ROOT.kBlue-6,
 'neutron':ROOT.kGreen-2,
 'gamma':ROOT.kRed-6,
 'other':ROOT.kCyan-8,
 'proton':ROOT.kBlack,
}

def sumAll2D(his, pairs, folder):
  H = None
  for i,pair in enumerate(pairs):
    vol = pair[1]
    for p in particles:
      temp = f.Get("%s/%s/%s/%s_%s_%s_%s" % (folder,vol,his[0],vol,p,folder[0:3],his[1]))
      if temp:
        h = temp.Clone()
      else:
        continue
      if h:
        if H == None:
          H = h
        else:
          H.Add(h)
  return H

def sumAll(name, pairs, folder):
  hs = ROOT.THStack()
  hMap = {}
  for i,pair in enumerate(pairs):
    vol = pair[1]
    for p in particles:
      h = f.Get("%s/%s/%s/%s_%s_%s_%s" % (folder, vol,his,vol,p,folder[0:3],his))
      if h:
        h.SetLineColor(colors[p]+2)
        if p in hMap.keys():
          hMap[p].Add(h)
        else:
          hMap[p] = h.Clone()
  for p in particles:
    if p in hMap.keys():
      hs.Add(hMap[p])
  return hs

def addToDict(d,h):
  for i in range(1,h.GetNbinsX()+1):
    name = h.GetXaxis().GetBinLabel(i)
    val = h.GetBinContent(i)
    if name in d.keys():
      d[name] += val
    else:
      d[name] = val

def getHsPairs(hs):
  pairs = []
  h = hs.GetStack().Last()
  for i in range(1,h.GetNbinsX()+1):
    pairs += [[h.GetBinContent(i),h.GetXaxis().GetBinLabel(i)]]
  return pairs

def plotSummary(outfolder, name, hs, leg, xaxis, yaxis, botMargin = None):
  canv = ROOT.TCanvas(name,name,1000,600)
  canv.SetTopMargin(0.07)
  canv.SetLeftMargin(0.10)
  if botMargin != None:
    canv.SetBottomMargin(botMargin)
  h = hs.GetStack().Last()
  hs.SetMaximum(1.3*h.GetMaximum())
  hs.Draw("hist")
  hs.GetXaxis().SetTitle(xaxis)
  hs.GetYaxis().SetTitle(yaxis)
  hs.GetYaxis().SetTitleOffset(0.7)
  leg.Draw()
  if AtlasStyle:
    ROOT.ATLASLabel(0.18,0.87,"Sim. internal",1)
    ROOT.myText(0.18,0.81,1,"#sqrt{s}=13 TeV, %s" % outfolder.replace("/",""))
  return canv

def plot(outfolder, name, hs, leg, xaxis, yaxis, label, folder, pdf = "", mat = None, ALL_STEPS = None):
  canv = ROOT.TCanvas(hs.GetName(),hs.GetName(),800,600)
  canv.SetLogx()
  canv.SetLogy()
  canv.SetTopMargin(0.07)
  canv.SetLeftMargin(0.14)
  h = hs.GetStack().Last()
  h.SetLineColor(18)
  h.SetFillColor(18)
  nx = h.GetNbinsX()
  integral = h.Integral(0,nx+1)
  h.SetMaximum(100*h.GetMaximum())
  h.Draw()
  h.GetXaxis().SetTitle(xaxis)
  h.GetYaxis().SetTitle(yaxis)
  h.GetYaxis().SetTitleOffset(1.0)
  # percentile markers
  percentileX = 1e-6
  percentileLines = []
  if (True):
    while(percentileX < 1):
      canv.Update()
      xmin = ROOT.gPad.GetUxmin()
      xmax = ROOT.gPad.GetUxmax()
      ymin = ROOT.gPad.GetUymin()
      ymax = ROOT.gPad.GetUymax()
      percentile = 100*h.Integral(0,h.FindBin(percentileX))/integral
      percentileLine = ROOT.TLine(percentileX,10**(ymin-0.15*(ymax-ymin)),percentileX,10**ymax)
      percentileLine.SetLineColor(ROOT.kGray+2)
      percentileLine.SetLineStyle(2)
      percentileLine.Draw()
      percentileLines += [percentileLine]
      percentileText = ROOT.TLatex(percentileX*0.15, 10**(ymin-0.118*(ymax-ymin)), "%2.2f%s" % (percentile,"%"))
      percentileText.SetTextColor(ROOT.kGray+2)
      percentileText.SetTextSize(0.025)
      percentileText.Draw()
      percentileLines += [percentileText]
      percentileX = percentileX * 100
  leg.Draw()
  if AtlasStyle:
    ROOT.ATLASLabel(0.18,0.87,"Sim. internal",1)
    ROOT.myText(0.18,0.81,1,"#sqrt{s}=13 TeV, %s" % outfolder.replace("/",""))
    ROOT.myText(0.18,0.75,1,label + (" (%2.1f%s)" % (100*integral/ALL_STEPS,"%")) if ALL_STEPS else "")
    ROOT.myText(0.18,0.69,ROOT.kGray+2,"under/over: %2.2f/%2.2f%s" % (100*h.GetBinContent(0)/integral,100*h.GetBinContent(nx+1)/integral,"%"))
  if mat and ("secondary" in xaxis) and (mat.name not in ["Lead","Iron","KaptonC","IBL_Fwd"]):
    canv.Update()
    xmin = ROOT.gPad.GetUxmin()
    xmax = ROOT.gPad.GetUxmax()
    ymin = ROOT.gPad.GetUymin()
    ymax = ROOT.gPad.GetUymax()
    lines = []
    for ET in mat.energyThresholds:
      line = ROOT.TLine(mat.energyThresholds[ET]/1e6,10**ymin,mat.energyThresholds[ET]/1e6,10**ymax)
      if ET != "proton":
        line.SetLineColor(colors[ET]+2)
      line.SetLineStyle(2)
      lines += [line]
      line.Draw()
  hs.Draw("same hist nostack")
  ROOT.gPad.RedrawAxis()
  if folder != "":
    canv.Print(outfolder+folder+name+".png")
  else:
    canv.Print(outfolder+name+".pdf" + pdf)

def plot2D(outfolder, name, h, xaxis, yaxis, label, label2, folder, pdf = "", ALL_STEPS = None):
  canv = ROOT.TCanvas(h.GetName(),h.GetName(),800,600)
  canv.SetRightMargin(0.20)
  canv.SetTopMargin(0.07)
  canv.SetLogz()
  nx = h.GetNbinsX()
  ny = h.GetNbinsY()
  integral = h.Integral(0,nx+1,0,ny+1)
  h.Draw("COLZ")
  h.GetXaxis().SetTitle(xaxis)
  h.GetYaxis().SetTitle(yaxis)
  h.GetZaxis().SetTitle("Steps")
  h.GetZaxis().SetTitleOffset(1.2)
  h.GetXaxis().SetNdivisions(505)
  if AtlasStyle:
    ROOT.ATLASLabel(0.18,0.87,"Sim. internal",1)
    ROOT.myText(0.18,0.81,1,"#sqrt{s}=13 TeV, %s" % outfolder.replace("/",""))
    ROOT.myText(0.18,0.75,1,label + (" (%2.1f%s)" % (100*integral/ALL_STEPS,"%")) if ALL_STEPS else "")
    ROOT.myText(0.18,0.69,1,label2)
  ROOT.gPad.RedrawAxis()
  if folder == "":
    canv.Print(outfolder+folder+name+".pdf" + pdf)
  else:
    canv.Print(outfolder+folder+name+".png")


if __name__ == '__main__':

  inputPath = sys.argv[1]
  f = ROOT.TFile(inputPath,"READ")

  f2 = open(sys.argv[2], 'r')

  outfolder = sys.argv[3]
  if not os.path.exists(outfolder):
      os.makedirs(outfolder)
  if not os.path.exists(outfolder+"/1Dplots"):
      os.makedirs(outfolder+"/1Dplots")
  if not os.path.exists(outfolder+"/2Dplots"):
      os.makedirs(outfolder+"/2Dplots")

  materials = getMaterials(f2)
  mats = {}
  for m in materials:
    mats[materials[m].name] = materials[m]

  stackVols = ROOT.THStack()
  stackMats = ROOT.THStack()
  stackVolsAll = ROOT.THStack()
  stackMatsAll = ROOT.THStack()

  leg = ROOT.TLegend(0.6,0.90-0.03*len(particles),0.9,0.90)
  leg.SetNColumns(2)
  leg.SetBorderSize(0)
  leg.SetFillColor(0)
  leg.SetFillStyle(0)
  leg.SetTextSize(0.045)

  leg_l = ROOT.TLegend(0.6,0.90-0.03*len(particles),0.9,0.90)
  leg_l.SetNColumns(2)
  leg_l.SetBorderSize(0)
  leg_l.SetFillColor(0)
  leg_l.SetFillStyle(0)
  leg_l.SetTextSize(0.045)

  ALL_STEPS = 0

  allMaterials = {}
  allVolumes = {}
  allMaterialsPerP = {}
  allVolumesPerP = {}

  for p in particles:
    hV = f.Get("volumes/summary/nStepts_%s" % p)
    hM = f.Get("materials/summary/nStepts_%s" % p)
    ALL_STEPS += hV.Integral()
    hVall = f.Get("volumes/summaryAll/nStepts_%s" % p)
    hMall = f.Get("materials/summaryAll/nStepts_%s" % p)
    addToDict(allVolumes, hVall)
    addToDict(allMaterials, hMall)
    volDict = {}
    matDict = {}
    addToDict(volDict, hVall)
    addToDict(matDict, hMall)
    allVolumesPerP[p] = volDict
    allMaterialsPerP[p] = matDict

  MAX = 30
  sorted_vol = sorted(allVolumes.items(), key=operator.itemgetter(1), reverse=True)
  sorted_mat = sorted(allMaterials.items(), key=operator.itemgetter(1), reverse=True)
  for p in particles:
    hV = ROOT.TH1D("hv"+p,"hv"+p,MAX+1,0,MAX+1)
    hM = ROOT.TH1D("hm"+p,"hm"+p,MAX+1,0,MAX+1)
    f1 = open('volumes.txt', 'w')
    A = sum(allVolumesPerP[p].values())
    for i,k in enumerate(sorted_vol):
      f1.write("%s\t%2.1f\n" % (k[0],100*k[1]/ALL_STEPS))
      if i < MAX:
        content = allVolumesPerP[p][k[0]] if k[0] in allVolumesPerP[p].keys() else 0
        # print k[0]," ",content
        hV.SetBinContent(i+1,content)
        A -= content
      elif i==MAX:
        hV.SetBinContent(i+1,A)
    f1.close()
    f2 = open('materials.txt', 'w')
    A = sum(allMaterialsPerP[p].values())
    for i,k in enumerate(sorted_mat):
      f2.write("%s\t%2.1f\n" % (k[0],100*k[1]/ALL_STEPS))
      if i < MAX:
        content = allMaterialsPerP[p][k[0]] if k[0] in allMaterialsPerP[p].keys() else 0
        # print k[0]," ",content
        hM.SetBinContent(i+1, content)
        A -= content
      elif i==MAX:
        hM.SetBinContent(i+1,A)
    f2.close()
    hM.SetFillColor(colors[p])
    hM.SetLineColor(colors[p]+2)
    hV.SetFillColor(colors[p])
    hV.SetLineColor(colors[p]+2)
    hM.Scale(100./ALL_STEPS)
    hV.Scale(100./ALL_STEPS)
    stackVolsAll.Add(hV)
    stackMatsAll.Add(hM)

  for p in particles:
    hV = f.Get("volumes/summary/nStepts_%s" % p)
    hM = f.Get("materials/summary/nStepts_%s" % p)
    hV.SetFillColor(colors[p])
    hV.SetLineColor(colors[p]+2)
    hM.SetFillColor(colors[p])
    hM.SetLineColor(colors[p]+2)
    hV.Scale(100./ALL_STEPS)
    hM.Scale(100./ALL_STEPS)
    stackVols.Add(hV)
    stackMats.Add(hM)
    leg.AddEntry(hV,"#font[42]{%s}" % p,"f")
    leg_l.AddEntry(hV,"#font[42]{%s}" % p,"l")
  tempH = ROOT.TH1F("temph","temph",1,0,1)
  tempH.SetFillColor(19)
  tempH.SetLineColor(19)
  leg_l.AddEntry(tempH,"#font[42]{sum}","f")

  volPairs = getHsPairs(stackVols)
  volPairs = sorted(volPairs, key = lambda x: -x[0])

  matPairs = getHsPairs(stackMats)
  matPairs = sorted(matPairs, key = lambda x: -x[0])

  canv = plotSummary(outfolder,"nSteps_volume", stackVols, leg, "Volume", "Relative steps [%]")
  canv.Print(outfolder+"nSteps_volume.pdf")
  canv.Print(outfolder+"nSteps_volume.png")
  canv = plotSummary(outfolder,"nSteps_material", stackMats, leg, "Material", "Relative steps [%]")
  canv.Print(outfolder+"nSteps_material.pdf")
  canv.Print(outfolder+"nSteps_material.png")
  # canv = plotSummary("nSteps_volume_all", stackVolsAll, leg, "", "Relative steps [%]", 0.35)
  # for i,k in enumerate(sorted_vol):
  #   if i == MAX:
  #     break
  #   stackVolsAll.GetXaxis().SetBinLabel(i+1,k[0])
  # stackVolsAll.GetXaxis().SetBinLabel(MAX+1,"other")
  # stackVolsAll.GetXaxis().LabelsOption("v")
  # canv.Print("nSteps_volume_all.pdf")
  canv = plotSummary(outfolder,"nSteps_material_all", stackMatsAll, leg, "", "Relative steps [%]", 0.35)
  for i,k in enumerate(sorted_mat):
    if i == MAX:
      break
    stackMatsAll.GetXaxis().SetBinLabel(i+1,k[0])
  stackMatsAll.GetXaxis().SetBinLabel(MAX+1,"other")
  stackMatsAll.GetXaxis().LabelsOption("v")
  canv.Print(outfolder+"nSteps_material_all.pdf")
  canv.Print(outfolder+"nSteps_material_all.png")

  # 1D Plots
  #---------

  hisNames = [
  'stepLength',
  'stepEnergyDeposit',
  'stepEnergyNonIonDeposit',
  'stepEnergySecondary',
  ]

  label = {
  'stepLength':'step length [mm]',
  'stepEnergyDeposit':'energy deposit [MeV]',
  'stepEnergyNonIonDeposit':'non-ionizing energy deposit [MeV]',
  'stepEnergySecondary':'secondary radiation energy [MeV]',
  }


  for his in hisNames:
    # VOLUMES
    hsAll = sumAll(his, volPairs, "volumes")
    plot(outfolder, "volumes_%s" % his, hsAll, leg_l, label[his], "steps", "ALL ATLAS", "", "(", None, ALL_STEPS)
    plot(outfolder, "volumes_%s" % his, hsAll, leg_l, label[his], "steps", "ALL ATLAS", "1Dplots/", "", None, ALL_STEPS)
    for i,pair in enumerate(volPairs):
      vol = pair[1]
      hs = ROOT.THStack()
      hsNotEmpty = False
      for p in particles:
        h = f.Get("volumes/%s/%s/%s_%s_vol_%s" % (vol,his,vol,p,his))
        if h:
          h.SetLineColor(colors[p]+2)
          hs.Add(h)
          hsNotEmpty = True
      if not hsNotEmpty:
        continue
      plot(outfolder, "volumes_%s_%s" % (his,vol), hs, leg_l, label[his], "steps", vol, "1Dplots/", "", None, ALL_STEPS)
      if i == (len(volPairs)-1):
        plot(outfolder, "volumes_%s" % his, hs, leg_l, label[his], "steps", vol, "", ")", None, ALL_STEPS)
      else:
        plot(outfolder, "volumes_%s" % his, hs, leg_l, label[his], "steps", vol, "", "", None, ALL_STEPS)

    # MATERIALS
    hsAll = sumAll(his, matPairs, "materials")
    plot(outfolder, "materials_%s" % his, hsAll, leg_l, label[his], "steps", "ALL ATLAS", "", "(", None, ALL_STEPS)
    plot(outfolder, "materials_%s" % his, hsAll, leg_l, label[his], "steps", "ALL ATLAS", "1Dplots/", "", None, ALL_STEPS)
    for i,pair in enumerate(matPairs):
      mat = pair[1]
      hs = ROOT.THStack()
      hsNotEmpty = False
      for p in particles:
        h = f.Get("materials/%s/%s/%s_%s_mat_%s" % (mat,his,mat,p,his))
        if h:
          h.SetLineColor(colors[p]+2)
          hs.Add(h)
          hsNotEmpty = True
      if not hsNotEmpty:
        if i == (len(matPairs)-1):
          ROOT.TCanvas("temp","temp",800,600).Print("materials_%s.pdf)" % his)
        continue
      plot(outfolder, "materials_%s_%s" % (his,mat), hs, leg_l, label[his], "steps", mat, "1Dplots/", "", mats[mat] if mat not in ["other","IBL_Fwd"] else None, ALL_STEPS)
      if i == (len(matPairs)-1):
        plot(outfolder, "materials_%s" % his, hs, leg_l, label[his], "steps", mat, "", ")", mats[mat] if mat not in ["other","IBL_Fwd"] else None, ALL_STEPS)
      else:
        plot(outfolder, "materials_%s" % his, hs, leg_l, label[his], "steps", mat, "", "", mats[mat] if mat not in ["other","IBL_Fwd"] else None, ALL_STEPS)


  ROOT.TGaxis().SetMaxDigits(4)
  ROOT.TGaxis().SetExponentOffset(0.0, -0.08, "x")

  # 2D plots
  #---------

  hisNames2D = [
  ['2DMaps','RZ'],
  ['2DMapsSmall','RZ_small'],
  ]  

  for his in hisNames2D:
    h2DAll = sumAll2D(his, volPairs, "volumes")
    plot2D(outfolder, "volumes_allParticles_%s" % his[0], h2DAll, "z [mm]", "r [mm]", "ALL ATLAS", "ALL particles", "", "(", ALL_STEPS)
    plot2D(outfolder, "volumes_allParticles_%s" % his[0], h2DAll, "z [mm]", "r [mm]", "ALL ATLAS", "ALL particles", "2Dplots/", "", ALL_STEPS)
    for i,pair in enumerate(volPairs):
      vol = pair[1]
      h2DAll = sumAll2D(his, [pair], "volumes")
      plot2D(outfolder, "volumes_allParticles_%s_%s" % (his[0], vol), h2DAll, "z [mm]", "r [mm]", vol, "ALL particles", "2Dplots/", "", ALL_STEPS)
      if i == (len(volPairs)-1):
        plot2D(outfolder, "volumes_allParticles_%s" % his[0], h2DAll, "z [mm]", "r [mm]", vol, "ALL particles", "", ")", ALL_STEPS)
      else:
        plot2D(outfolder, "volumes_allParticles_%s" % his[0], h2DAll, "z [mm]", "r [mm]", vol, "ALL particles","", "", ALL_STEPS)
      for p in particles:
        h = f.Get("volumes/%s/%s/%s_%s_vol_%s" % (vol,his[0],vol,p,his[1]))
        if h:
          if i == 0 and p == particles[0]:
            plot2D(outfolder, "volumes_%s" % his[0], h, "z [mm]", "r [mm]", vol, p, "", "(", ALL_STEPS)            
          elif i == (len(volPairs)-1) and p == particles[-1]:
            plot2D(outfolder, "volumes_%s" % his[0], h, "z [mm]", "r [mm]", vol, p, "", ")", ALL_STEPS)
          else:
            plot2D(outfolder, "volumes_%s" % his[0], h, "z [mm]", "r [mm]", vol, p, "", "", ALL_STEPS)
        elif i == (len(volPairs)-1) and p == particles[-1]:
          ROOT.TCanvas("temp","temp",800,600).Print("volumes_%s.pdf)" % his[0])
        elif i == 0 and p == particles[0]:
          ROOT.TCanvas("temp","temp",800,600).Print("volumes_%s.pdf(" % his[0])

  for his in hisNames2D:
    h2DAll = sumAll2D(his, matPairs, "materials")
    plot2D(outfolder, "materials_allParticles_%s" % his[0], h2DAll, "z [mm]", "r [mm]", "ALL ATLAS", "ALL particles", "", "(", ALL_STEPS)
    plot2D(outfolder, "materials_allParticles_%s" % his[0], h2DAll, "z [mm]", "r [mm]", "ALL ATLAS", "ALL particles", "2Dplots/", "", ALL_STEPS)
    for i,pair in enumerate(matPairs):
      mat = pair[1]
      h2DAll = sumAll2D(his, [pair], "materials")
      plot2D(outfolder, "materials_allParticles_%s_%s" % (his[0],mat), h2DAll, "z [mm]", "r [mm]", mat, "ALL particles", "2Dplots/", "", ALL_STEPS)
      if i == (len(matPairs)-1):
        plot2D(outfolder, "materials_allParticles_%s" % his[0], h2DAll, "z [mm]", "r [mm]", mat, "ALL particles","", ")", ALL_STEPS)
      else:
        plot2D(outfolder, "materials_allParticles_%s" % his[0], h2DAll, "z [mm]", "r [mm]", mat, "ALL particles","", "", ALL_STEPS)
      for p in particles:
        h = f.Get("materials/%s/%s/%s_%s_mat_%s" % (mat,his[0],mat,p,his[1]))
        if h:
          if i == 0 and p == particles[0]:
            plot2D(outfolder, "materials_%s" % his[0], h, "z [mm]", "r [mm]", mat, p, "", "(", ALL_STEPS)            
          elif i == (len(matPairs)-1) and p == particles[-1]:
            plot2D(outfolder, "materials_%s" % his[0], h, "z [mm]", "r [mm]", mat, p, "", ")", ALL_STEPS)
          else:
            plot2D(outfolder, "materials_%s" % his[0], h, "z [mm]", "r [mm]", mat, p, "", "", ALL_STEPS)
        elif i == (len(matPairs)-1) and p == particles[-1]:
          ROOT.TCanvas("temp","temp",800,600).Print("materials_%s.pdf)" % his[0])
        elif i == 0 and p == particles[0]:
          ROOT.TCanvas("temp","temp",800,600).Print("materials_%s.pdf(" % his[0])








