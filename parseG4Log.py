import G4Material
from G4Material import unitMap

from copy import deepcopy
from collections import defaultdict

def main(args):
  f = open(args[0], 'r')

  materials = {}

  materialLine = 0

  index = -1

  for x in f:
    a = x.split()
    if "used in the geometry" in x:
      # print "- Found new material, index: ", a[4]
      materialLine += 1
      mat = deepcopy(G4Material.G4Material(index=a[4]))
      materials[a[4]] = mat
      index = a[4]
      continue
    elif materialLine == 1:
      # print "- named: ", a[4]
      materialLine += 1
      materials[index].setName(a[4])
      continue
    elif materialLine == 2:
      assert len(a) == 17, len(a)
      assert a[5] == "gamma"
      assert a[8] == "e-"
      assert a[11] == "e+"
      assert a[14] == "proton"
      # print "- Range Cuts:"
      for i in range(4):
        # print "%6s: %10.3f um" % (a[5+3*i],unitMap[a[7+3*i]]*float(a[6+3*i]))
        materials[index].rangeCuts[a[5+3*i]] = unitMap[a[7+3*i]]*float(a[6+3*i])
      materialLine += 1
      continue
    elif materialLine == 3:
      assert len(a) == 17, len(a)
      assert a[5] == "gamma"
      assert a[8] == "e-"
      assert a[11] == "e+"
      assert a[14] == "proton"
      # print "- Energy Thresholds:"
      for i in range(4):
        # print "%6s: %10.3f eV" % (a[5+3*i],unitMap[a[7+3*i]]*float(a[6+3*i]))
        materials[index].energyThresholds[a[5+3*i]] = unitMap[a[7+3*i]]*float(a[6+3*i])
      materialLine += 1
      continue
    elif materialLine == 4:
      assert a[2]=="Region(s)"
      materialLine += 1
    elif materialLine == 5:
      # print "- Regions: ", a[2:]
      # print "-----------------"
      materials[index].regions = a[2:]
      materialLine += 1
    else:
      materialLine = 0
      index = -1


  matClasses = defaultdict(list)
  for index,mat in materials.iteritems():
    matClasses[mat.getEnergyThresholdsString()] += [mat]

  print "materials: ",len(materials)
  print "classes: ",len(matClasses)

  for key, value in sorted(matClasses.iteritems(), key=lambda (k,v): -len(v)):
      print "----------- same materials (%s times) -----------" % len(value)
      for mat in value:
        print mat




if __name__ == '__main__':
  import sys
  main(sys.argv[1:])
